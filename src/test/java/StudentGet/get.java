package StudentGet;
import org.testng.annotations.Test;

import baseClass.beforeClass;
import io.restassured.*;
import io.restassured.response.Response;
public class get extends beforeClass {
	@Test
	public void getStudentInformation() {
		Response response = RestAssured.given()
		//When is used to provide get post delete put request
		.when()
		.get("/list");
		// print the API response
		System.out.println(response.body().prettyPrint());
		//Validate the response 
		RestAssured.given()
		.when().get("/list")
		// is used for post validation and used for further analysis on response
		.then()
		.statusCode(200);
	
		
	}
	
	@Test
	public void getFirstStudentResponse() {
		RestAssured.given()
		.when()
		.get("/1")
		.then()
		.statusCode(200);
	}
	
	}
